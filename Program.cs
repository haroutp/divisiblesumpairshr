﻿using System;

namespace DivisibleSumPairs
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(divisibleSumPairs( 6, 3, new int[]{1, 3, 2, 6, 1, 2}));
        }

        // Complete the divisibleSumPairs function below.
        static int divisibleSumPairs(int n, int k, int[] ar) {
            int count = 0;

            for(int i = 0; i < ar.Length; i++){
                for(int j = i; j < ar.Length; j++){
                    if(i == j){
                        continue;
                    }
                    if((ar[i] + ar[j]) % k == 0 ){
                        count++;
                    }
                }
            }
            return count;
        }
    }
}
